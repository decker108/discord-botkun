function postForm(url = ``, data = {}) {
    return fetch(url, {
        method: "POST",
        mode: "cors",
        cache: "no-cache",
        credentials: "same-origin",
        headers: {
             "Content-Type": "application/x-www-form-urlencoded",
        },
        redirect: "follow",
        referrer: "no-referrer",
        body: data,
    })
    .then(response => response.json());
}

function setErrorMsg(message) {
    const formMsg = document.querySelector(".form-msg");
    formMsg.innerText = message;
    formMsg.classList.add("error-msg");
}

function resetErrorMsg() {
    const formMsg = document.querySelector(".form-msg");
    formMsg.innerText = "";
    formMsg.classList.remove("error-msg");
}

function handleError(error) {
    console.error(error);
    setErrorMsg(error);
}

function submitTrackMangaForm() {
    resetErrorMsg();
    const name = encodeURIComponent(document.querySelector("#track-manga-form-name").value);
    const rssUrl = encodeURIComponent(document.querySelector("#track-manga-form-url").value);
    const mangaLink = encodeURIComponent(document.querySelector("#track-manga-form-mangalink").value);
    const url = `/api/manga/track`;
    console.log(`Submitting form with name ${name} and rss-url ${rssUrl} to ${url}`);

    postForm(url, `name=${name}&url=${rssUrl}&mangalink=${mangaLink}`).then(response => {
        if (!response.error) {
            fetch("/api/manga")
                .then((response) => response.json())
                .then(jsonData => updateMangaList(jsonData))
                .catch(error => handleError);
        } else {
            handleError(`Non-OK response: ${response.error}`)
        }
    }).catch(error => handleError);
}

function updateMangaList(responseJson) {
    resetErrorMsg();
    const listElem = document.querySelector(".tracked-manga-list");
    listElem.innerHTML = null;
    const mangaList = responseJson || [];
    mangaList.forEach(elem => {
        const listItemElem = document.createElement("li");
        listItemElem.textContent = elem.name;
        listElem.appendChild(listItemElem);
    });
}

fetch("/api/manga")
    .then((response) => response.json())
    .then(jsonData => updateMangaList(jsonData))
    .catch(error => handleError);