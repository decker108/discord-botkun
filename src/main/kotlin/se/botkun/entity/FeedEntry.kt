package se.botkun.entity

import java.time.ZonedDateTime

data class FeedEntry(
        val id: String, // Unique id based on a sha1 sum of the release name from the RSS feed
        val name: String,
        val rawTitle: String,
        val url: String,
        val updateDate: ZonedDateTime,
        val seriesId: String
)