package se.botkun.entity

import java.time.ZonedDateTime

class TrackedManga(
        val id: Int,
        val name: String,
        val url: String,
        val addDate: ZonedDateTime,
        val lastUpdateDate: ZonedDateTime,
        val seriesId: String,
        val mangaLink: String? = null,
        val lastSeenEntryId: String? = null
)