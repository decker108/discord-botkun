package se.botkun.entity

import java.time.ZonedDateTime

class TrackedAnime(val id: Int, val name: String, val url: String, val addDate: ZonedDateTime, val lastUpdateDate: ZonedDateTime)