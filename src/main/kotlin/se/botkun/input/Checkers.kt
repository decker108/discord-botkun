package se.botkun.input

import io.sentry.Sentry
import mu.KLogging
import org.jdbi.v3.core.Jdbi
import se.botkun.ConfigBundle
import se.botkun.entity.FeedEntry
import se.botkun.entity.TrackedManga
import se.botkun.output.Embed
import se.botkun.persistence.MangaRepository
import java.net.URL
import java.util.stream.Collectors

class MangaUpdatesChecker(dbConn: Jdbi, private val webHookFn: (String, List<Embed>) -> Unit, private val configBundle: ConfigBundle) : Runnable, KLogging() {
    private val mangaRepository = MangaRepository(dbConn)

    override fun run() {
        logger.info("Checking MangaUpdates")
        try {
            val mangaInFeed = parseFeed(URL(configBundle.muFeedUrl))
            logger.debug("Manga in feed: " + mangaInFeed.map { it.name }.toList())
            val updatedManga = findUpdatedManga(mangaInFeed)
            if (updatedManga.isNotEmpty()) {
                updatedManga.forEach { mangaRepository.updateWithLatestRelease(it.first) }
                val msg = "The following manga have new chapters: " + updatedManga.map { it.first.name }.toList()
                val embeds = updatedManga
                        .filter { !it.second.mangaLink.isNullOrEmpty() }
                        .map { Embed(it.first.name, it.second.mangaLink!!, it.first.rawTitle) }
                webHookFn(msg, embeds)
                logger.info(msg)
            }
            logger.info("Done checking MangaUpdates")
        } catch (e: Exception) {
            logger.error(e) { "Error checking MangaUpdates for new chapters" }
            Sentry.capture(e)
        }
    }

    private fun findUpdatedManga(feed: List<FeedEntry>): List<Pair<FeedEntry, TrackedManga>> {
        //Get list of tracked manga, compare with manga from feed, return list of matches
        val storedMangaMap = mangaRepository.findAll()
                .stream()
                .collect(Collectors.toMap({ it.seriesId }, { it: TrackedManga -> it }))
        return feed
                .filter {
                    storedMangaMap.containsKey(it.seriesId) && it.id != storedMangaMap[it.seriesId]?.lastSeenEntryId
                }
                .map { Pair(it, storedMangaMap[it.seriesId]!!) }
                .toList()
    }
}