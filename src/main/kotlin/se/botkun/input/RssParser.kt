package se.botkun.input

import com.rometools.rome.feed.synd.SyndEntryImpl
import com.rometools.rome.feed.synd.SyndFeed
import com.rometools.rome.io.SyndFeedInput
import io.sentry.Sentry
import mu.KLogging
import okhttp3.OkHttpClient
import okhttp3.Request
import org.apache.commons.codec.digest.DigestUtils
import org.apache.commons.lang3.StringUtils
import se.botkun.entity.FeedEntry
import java.net.URL
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime

private val logger = KLogging().logger

fun getSyndFeedForUrl(url: URL): SyndFeed? {
    val client = OkHttpClient()
    client.newCall(Request.Builder().url(url).get().build()).execute().use { response ->
        return try {
            if ((200..299).contains(response.code)) {
                val string = response.body?.charStream()
                SyndFeedInput().build(string)
            } else {
                logger.error { "Received non-200 response: ${response.code}" }
                null
            }
        } catch (e: Exception) {
            Sentry.capture(e)
            logger.error("Error fetching RSS feed for URL $url", e)
            null
        }
    }
}

fun parseFeed(url: URL): List<FeedEntry> {
    val listOfEntries = getSyndFeedForUrl(url)?.entries ?: emptyList()
    return listOfEntries
            .filter { !it?.link.isNullOrBlank() }
            .map {
                val syndEntry = it as SyndEntryImpl
                val cleanedTitle = parseEntryTitle(syndEntry.title)
                val rawTitle = syndEntry.title.trim()
                val seriesId = parseEntryId(syndEntry.link!!)
                val entryId = DigestUtils.sha1Hex(rawTitle)
                FeedEntry(entryId, cleanedTitle, rawTitle,
                        syndEntry.link ?: "", getUpdateDate(syndEntry), seriesId)
            }
}

private fun parseEntryTitle(title: String?): String {
    return (title ?: "")
            .replaceFirst("[HorribleSubs] ", "")
            .replaceFirst(Regex("\\[.*]"), "")
            .replace(" .mkv", "")
            .replaceAfterLast("v.", "")
            .replaceAfterLast("c.", "")
            .replace(" v.", "")
            .replace(" c.", "")
            .trim()
}

val rssLinkPrefixes = arrayOf(
        "http://www.mangaupdates.com/series.html?id=",
        "https://www.mangaupdates.com/series.html?id="
)

private fun parseEntryId(link: String): String {
    if (link.startsWith("magnet")) {
        return link.substringBefore("&amp;").removePrefix("magnet:?xt=urn:btih:")
    }
    return StringUtils.replaceEach(link, rssLinkPrefixes,
            rssLinkPrefixes.map { "" }.toTypedArray())
}

private fun getUpdateDate(syndEntry: SyndEntryImpl): ZonedDateTime {
    val utcZone = ZoneId.of("Z")
    val maybeUpdateDate = syndEntry.updatedDate
    if (maybeUpdateDate != null) {
        return maybeUpdateDate.toInstant().atZone(utcZone)
    }
    val description = syndEntry.description?.value
    return if (!description.isNullOrBlank() && description.subSequence(0, 10)
                    .matches(Regex("\\d{4}-\\d{2}-\\d{2}"))) {
        val now = ZonedDateTime.now(utcZone)
        ZonedDateTime.of(
                LocalDateTime.of(description.substring(0, 4).toInt(),
                        description.substring(5, 7).toInt(),
                        description.substring(8, 10).toInt(),
                        now.hour,
                        now.minute,
                        now.second),
                utcZone
        )
    } else ZonedDateTime.now(utcZone)
}
