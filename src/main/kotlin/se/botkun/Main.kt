package se.botkun

import io.sentry.Sentry
import mu.KLogging
import org.apache.commons.dbcp2.BasicDataSource
import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.core.kotlin.KotlinPlugin
import org.jdbi.v3.postgres.PostgresPlugin
import se.botkun.input.MangaUpdatesChecker
import se.botkun.output.executeWebhook
import se.botkun.persistence.ZonedDateTimeMapper
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

private val scheduler = Executors.newScheduledThreadPool(1)

private val log = KLogging().logger

fun setupMUChecker(dbConn: Jdbi, configBundle: ConfigBundle) {
    scheduler.scheduleAtFixedRate(MangaUpdatesChecker(dbConn, ::executeWebhook, configBundle),
            1, 10, TimeUnit.MINUTES)
    log.info("Started MU checker")
}

internal fun setupDBConnector(): Jdbi {
    val user = System.getenv("BOTKUN_DB_USER")!!
    val password = System.getenv("BOTKUN_DB_PASSWORD")!!
    val url = "jdbc:postgresql://localhost/botkundb?user=$user&password=$password&ssl=false"
    val dataSource = BasicDataSource()
    dataSource.url = url
    val jdbi = Jdbi.create(dataSource)
            .installPlugin(PostgresPlugin())
            .installPlugin(KotlinPlugin())
            .registerColumnMapper(ZonedDateTimeMapper())
    return jdbi!!
}

internal fun checkForRequiredEnvVars() {
    val envVars = System.getenv()
    listOf(
            "BOTKUN_DB_USER",
            "BOTKUN_DB_PASSWORD",
            "DISCORD_WEBHOOK_URL",
            "DISCORD_WEBHOOK_ID",
            "DISCORD_WEBHOOK_TOKEN",
            "SENTRY_DSN"
    ).filter { !envVars.containsKey(it) }
            .any { throw RuntimeException("Env var $it has not been set") }
}

/**
 * Sentry relies on the SENTRY_DSN env var to be set.
 */
fun setupSentry() {
    Sentry.init()
}

fun main(@Suppress("UNUSED_PARAMETER") args: Array<String>) {
    checkForRequiredEnvVars()
    setupSentry()
    val configBundle = ConfigBundle(System.getenv("BOTKUN_RSS_TEST_URL") ?: "https://www.mangaupdates.com/rss.php")
    log.info("Active configuration: $configBundle")
    val dbConn = setupDBConnector()
    setupMUChecker(dbConn, configBundle)
}
