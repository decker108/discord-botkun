package se.botkun.persistence

import org.jdbi.v3.core.mapper.ColumnMapper
import org.jdbi.v3.core.statement.StatementContext
import java.sql.ResultSet
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.*

class ZonedDateTimeMapper: ColumnMapper<ZonedDateTime> {
    override fun map(rs: ResultSet?, col: Int, ctx: StatementContext?): ZonedDateTime {
        val timeStamp = rs?.getTimestamp(col)
        val start: Calendar = Calendar.getInstance()
        start.timeInMillis = timeStamp?.time ?: 0
        return ZonedDateTime.ofInstant(start.toInstant(), ZoneId.of("Z"))
    }
}