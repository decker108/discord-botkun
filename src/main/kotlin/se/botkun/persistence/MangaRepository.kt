package se.botkun.persistence

import io.sentry.Sentry
import mu.KLogging
import org.jdbi.v3.core.HandleCallback
import org.jdbi.v3.core.HandleConsumer
import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.core.kotlin.mapTo
import org.jdbi.v3.core.statement.UnableToExecuteStatementException
import se.botkun.entity.FeedEntry
import se.botkun.entity.TrackedManga
import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.*

class MangaRepository(private val jdbiConnection: Jdbi): KLogging() {
    private val defaultUpdateDate = ZonedDateTime.ofInstant(Instant.EPOCH, ZoneId.of("Z"))

    fun create(name: String, url: String, seriesId: String, mangaLink: String? = null): TrackedManga {
        try {
            return jdbiConnection.withHandle(HandleCallback<TrackedManga, Exception> { handle ->
                try {
                    val sql = """INSERT INTO tracked_manga(name, url, seriesId, mangaLink)
                         VALUES (:name, :url, :seriesId, :mangaLink)""".trimMargin()
                    val generatedId = handle.createUpdate(sql)
                            .bind("name", name)
                            .bind("url", url)
                            .bind("seriesId", seriesId)
                            .bind("mangaLink", mangaLink)
                            .executeAndReturnGeneratedKeys("id")
                            .mapTo(Int::class.java)
                            .findOnly()
                    if (generatedId <= 0) {
                        throw RuntimeException("Error: No rows inserted")
                    }
                    return@HandleCallback TrackedManga(generatedId, name, url,
                            ZonedDateTime.now(ZoneId.of("Z")), defaultUpdateDate, seriesId)
                } catch (e: UnableToExecuteStatementException) {
                    val msg = when {
                        e.message?.contains("tracked_manga_name_key")
                                ?: false -> "Found manga with duplicate name $name"
                        e.message?.contains("tracked_manga_url_key")
                                ?: false -> "Found manga with duplicate url $url"
                        else -> e.message
                    }
                    throw RuntimeException(msg)
                }
            })!!
        } catch (e: Exception) {
            Sentry.capture(e)
            logger.error(e) { "Error creating manga" }
            throw RuntimeException(e)
        }
    }

    fun findByName(name: String): Optional<TrackedManga> {
        try {
            return jdbiConnection.withHandle(HandleCallback<Optional<TrackedManga>, Exception> { handle ->
                val sql = "SELECT * FROM tracked_manga WHERE name = :name LIMIT 1"
                return@HandleCallback handle.createQuery(sql)
                        .bind("name", name)
                        .mapTo<TrackedManga>()
                        .findFirst()
            })
        } catch (e: Exception) {
            Sentry.capture(e)
            logger.error(e) { "Error finding manga by name" }
            throw RuntimeException(e)
        }
    }

    fun findById(id: Int): Optional<TrackedManga> {
        try {
            return jdbiConnection.withHandle(HandleCallback<Optional<TrackedManga>, Exception> { handle ->
                val sql = "SELECT * FROM tracked_manga WHERE id = :id LIMIT 1"
                return@HandleCallback handle.createQuery(sql)
                        .bind("id", id)
                        .mapTo<TrackedManga>()
                        .findFirst()
            })
        } catch (e: Exception) {
            Sentry.capture(e)
            logger.error(e) { "Error finding manga by id" }
            throw RuntimeException(e)
        }
    }

    fun deleteById(id: Int) {
        try {
            jdbiConnection.useHandle(HandleConsumer<Exception> { handle ->
                val sql = "DELETE FROM tracked_manga WHERE id = :id"
                val deletedRows = handle.createUpdate(sql).bind("id", id).execute()
                if (deletedRows != 1) {
                    throw RuntimeException("No manga to delete found for id $id")
                }
            })
        } catch (e: Exception) {
            Sentry.capture(e)
            logger.error(e) { "Error deleting manga" }
            throw RuntimeException(e)
        }
    }

    fun deleteByEntryId(seriesId: String) {
        try {
            jdbiConnection.useHandle(HandleConsumer<Exception> { handle ->
                val sql = "DELETE FROM tracked_manga WHERE seriesid = :seriesId"
                val deletedRows = handle.createUpdate(sql).bind("seriesId", seriesId).execute()
                if (deletedRows != 1) {
                    throw RuntimeException("No manga to delete found for seriesId $seriesId")
                }
            })
        } catch (e: Exception) {
            Sentry.capture(e)
            logger.error(e) { "Error deleting manga" }
            throw RuntimeException(e)
        }
    }

    fun update(mangaToUpdate: TrackedManga): TrackedManga {
        try {
            return jdbiConnection.withHandle(HandleCallback<TrackedManga, Exception> { handle ->
                val sql = """UPDATE tracked_manga
                     SET name = :name, url = :url, lastUpdateDate = NOW()
                     WHERE id = :id"""
                var updateObj = handle.createUpdate(sql)
                        .bind("id", mangaToUpdate.id)
                        .bind("name", mangaToUpdate.name)
                        .bind("url", mangaToUpdate.url)
                        .bind("seriesid", mangaToUpdate.seriesId)
                if (!mangaToUpdate.mangaLink.isNullOrEmpty()) {
                    updateObj = updateObj.bind("mangalink", mangaToUpdate.mangaLink)
                }
                if (!mangaToUpdate.lastSeenEntryId.isNullOrEmpty()) {
                    updateObj = updateObj.bind("lastseenentryid", mangaToUpdate.lastSeenEntryId)
                }
                if (updateObj.execute() != 1) {
                    throw RuntimeException("No manga to update found for id ${mangaToUpdate.id}")
                }
                return@HandleCallback mangaToUpdate
            })
        } catch (e: Exception) {
            Sentry.capture(e)
            logger.error(e) { "Error updating manga" }
            throw RuntimeException(e)
        }
    }

    fun findAll(): List<TrackedManga> {
        try {
            return jdbiConnection.withHandle(HandleCallback<List<TrackedManga>, Exception> { handle ->
                val sql = "SELECT * FROM tracked_manga"
                return@HandleCallback handle.createQuery(sql)
                        .mapTo<TrackedManga>()
                        .list()
            })
        } catch (e: Exception) {
            Sentry.capture(e)
            logger.error(e) { "Error finding all manga" }
            throw RuntimeException(e)
        }
    }

    fun updateWithLatestRelease(manga: FeedEntry) {
        try {
            jdbiConnection.useHandle(HandleConsumer<Exception> { handle ->
                val sql = """
                        UPDATE tracked_manga
                        SET lastupdatedate = :updateDate,
                            lastseenentryid = :lastSeenEntryId
                        WHERE id = (SELECT id FROM tracked_manga WHERE seriesid = :seriesId)
                    """
                val updatedRows = handle.createUpdate(sql)
                        .bind("updateDate", manga.updateDate.toInstant())
                        .bind("seriesId", manga.seriesId)
                        .bind("lastSeenEntryId", manga.id)
                        .execute()
                if (updatedRows != 1) {
                    logger.warn { "Failed to update release info for manga ${manga.name} with id ${manga.id}" }
                }
            })
        } catch (e: Exception) {
            Sentry.capture(e)
            logger.error(e) { "Error updating last update date" }
            throw RuntimeException(e)
        }
    }
}