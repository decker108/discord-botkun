package se.botkun.persistence

import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.core.kotlin.mapTo
import se.botkun.entity.TrackedAnime
import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.*


class AnimeRepository(private val jdbiConnection: Jdbi) {
    private val defaultUpdateDate = ZonedDateTime.ofInstant(Instant.EPOCH, ZoneId.of("Z"))

    fun create(name: String, url: String): TrackedAnime {
        jdbiConnection.open().use { handle ->
            val sql = "INSERT INTO tracked_manga(name, url) VALUES (:name, :url)"
            val generatedId = handle.createUpdate(sql)
                    .bind("name", name)
                    .bind("url", url)
                    .executeAndReturnGeneratedKeys("id")
                    .mapTo(Int::class.java)
                    .findOnly()
            if (generatedId <= 0) {
                throw RuntimeException("Error: No rows inserted")
            }
            return TrackedAnime(generatedId, name, url,
                    ZonedDateTime.now(ZoneId.of("Z")), defaultUpdateDate)
        }
    }

    fun findByName(name: String): Optional<TrackedAnime> {
        jdbiConnection.open().use { handle ->
            val sql = "SELECT * FROM tracked_manga WHERE name = :name LIMIT 1"
            return handle.createQuery(sql)
                    .bind("name", name)
                    .mapTo<TrackedAnime>()
                    .findFirst()
        }
    }
    fun findById(id: Int): Optional<TrackedAnime> {
        jdbiConnection.open().use { handle ->
            val sql = "SELECT * FROM tracked_manga WHERE id = :id LIMIT 1"
            return handle.createQuery(sql)
                    .bind("id", id)
                    .mapTo<TrackedAnime>()
                    .findFirst()
        }
    }

    fun update(mangaToUpdate: TrackedAnime): TrackedAnime {
        jdbiConnection.open().use { handle ->
            val sql = """UPDATE tracked_manga
             SET name = :name, url = :url, lastUpdateDate = NOW()
             WHERE id = :id"""
            val updatedRows = handle.createUpdate(sql)
                    .bind("id", mangaToUpdate.id)
                    .bind("name", mangaToUpdate.name)
                    .bind("url", mangaToUpdate.url)
                    .execute()
            if (updatedRows != 1) {
                throw RuntimeException("No manga to update found for id ${mangaToUpdate.id}")
            }
            return mangaToUpdate
        }
    }

    fun deleteById(id: Int) {
        jdbiConnection.open().use { handle ->
            val sql = "DELETE FROM tracked_manga WHERE id = :id"
            val deletedRows = handle.createUpdate(sql).bind("id", id).execute()
            if (deletedRows != 1) {
                throw RuntimeException("No manga to delete found for id $id")
            }
        }
    }
}