package se.botkun.output

data class Embed(val title: String, val url: String, val description: String?, val type: String = "rich")