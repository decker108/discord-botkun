package se.botkun.output

import com.fasterxml.jackson.databind.ObjectMapper
import io.sentry.Sentry
import mu.KLogging
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import java.net.URL

private val jsonMediaType = "application/json".toMediaTypeOrNull()

fun executeWebhook(message: String, embeds: List<Embed> = emptyList()) {
    val discordApiUrl = System.getenv("DISCORD_WEBHOOK_URL") ?: "https://discord.com/api"
    val webhookId = System.getenv("DISCORD_WEBHOOK_ID") ?: ""
    val webhookToken = System.getenv("DISCORD_WEBHOOK_TOKEN") ?: ""
    val url = URL("$discordApiUrl/webhooks/$webhookId/$webhookToken?wait=true")
    executeWebhook(message, url, embeds)
}

fun executeWebhook(message: String, discordUrl: URL, embeds: List<Embed>) {
    val logger = KLogging().logger
    val jsonString = ObjectMapper().writeValueAsString(mapOf(
            "content" to message,
            "username" to "Botkun",
            "embeds" to embeds
    ))

    val client = OkHttpClient()
    val request = Request.Builder()
            .url(discordUrl)
            .post(jsonString.toRequestBody(jsonMediaType))
            .build()

    client.newCall(request).execute().use { response ->
        try {
            if ((200..299).contains(response.code)) {
                logger.info("Successfully sent message to Discord: '$message'")
            } else {
                val responseMsg = response.code.toString() + " " + response.message + " " + response.body!!.string()
                logger.error("Received non-OK response (${response.code}) for webhook request, response message was: $responseMsg")
            }
        } catch (e: Exception) {
            Sentry.capture(e)
            logger.error("Error calling Discord", e)
        }
    }
}