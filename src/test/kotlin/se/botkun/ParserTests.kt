package se.botkun

import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.core.WireMockConfiguration.options
import com.github.tomakehurst.wiremock.junit.WireMockRule
import org.apache.commons.io.FileUtils
import org.junit.Rule
import org.junit.Test
import se.botkun.input.parseFeed
import java.io.File
import java.net.URL
import java.time.LocalDate
import kotlin.test.*

class RssParserTests {

    @Rule
    @JvmField
    val wireMockRule = WireMockRule(options().port(15000))

    @Test
    fun shouldParseMURssFeedIntoFeedEntryList() {
        setupMockServer("/example_mu_feed.xml", wireMockRule, javaClass)
        val feeds = parseFeed(URL("http://localhost:15000/example_mu_feed.xml"))
        assertEquals(29, feeds.size)
        assertTrue(feeds.all { it.name.isNotEmpty() })
        assertNull(feeds.find { it.name == "Together with Bell" })
        val feedEntry = feeds.find { it.name == "Bokutachi ga Yarimashita" }
        assertNotNull(feedEntry)
        assertEquals("https://www.mangaupdates.com/series.html?id=121087", feedEntry.url)
        assertEquals("121087", feedEntry.seriesId)
        assertEquals("6c09738a561861d70e97cfea1cb9fd7c35dd9199", feedEntry.id)
    }

    @Test
    fun shouldParseHSRssFeedIntoFeedEntryList() {
        setupMockServer("/example_hs_feed.xml", wireMockRule, javaClass)
        val feeds = parseFeed(URL("http://localhost:15000/example_hs_feed.xml"))
        assertEquals(50, feeds.size)
        assertTrue(feeds.all { it.name.isNotEmpty() && it.url.isNotEmpty() })
    }

    @Test
    fun parsingFeedTwiceShouldGiveDifferentDateTimesButSameEntryIds() {
        val exampleSeriesId = "86994"
        val expectedFeedEntryId = "28a5a48610d7c8f5186614d1652f9077bc768c00"
        setupMockServer("/example_mu_feed2.xml", wireMockRule, javaClass)

        val feed = parseFeed(URL("http://localhost:15000/example_mu_feed2.xml")).find { it.seriesId == exampleSeriesId }!!

        assertEquals(expectedFeedEntryId, feed.id)
        assertEquals(LocalDate.of(2018,12,29), feed.updateDate.toLocalDate())

        Thread.sleep(1001)

        val feed2 = parseFeed(URL("http://localhost:15000/example_mu_feed2.xml")).find { it.seriesId == exampleSeriesId }!!

        assertEquals(expectedFeedEntryId, feed2.id)
        assertEquals(LocalDate.of(2018,12,29), feed2.updateDate.toLocalDate())
        assertNotEquals(feed.updateDate, feed2.updateDate)
    }
}

fun setupMockServer(url: String, wireMockRule: WireMockRule, clazz: Class<Any>) {
    val uri = clazz.getResource(url).toURI()
    val feedContents = FileUtils.readFileToString(File(uri), "utf-8")
    wireMockRule.stubFor(WireMock.get(WireMock.urlPathMatching(url))
            .willReturn(WireMock.ok(feedContents)))
}
