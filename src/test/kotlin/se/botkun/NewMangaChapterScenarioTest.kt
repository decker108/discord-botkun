package se.botkun

import com.github.tomakehurst.wiremock.client.WireMock.*
import com.github.tomakehurst.wiremock.core.WireMockConfiguration.options
import com.github.tomakehurst.wiremock.junit.WireMockRule
import mu.KLogging
import org.apache.commons.io.FileUtils
import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.core.kotlin.KotlinPlugin
import org.jdbi.v3.postgres.PostgresPlugin
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import se.botkun.input.MangaUpdatesChecker
import se.botkun.output.Embed
import se.botkun.persistence.MangaRepository
import se.botkun.persistence.ZonedDateTimeMapper
import java.io.File
import kotlin.test.assertEquals

class NewMangaChapterScenarioTest : KLogging() {
    private lateinit var dbConn: Jdbi

    @Rule
    @JvmField
    val wireMockRule = WireMockRule(options().port(15000))

    @Before
    fun setUp() {
        dbConn = setupDBConnector()
        dbConn.useHandle<RuntimeException> {
            it.createUpdate("DELETE FROM tracked_manga WHERE name = :name")
                    .bind("name", "Bokutachi ga Yarimashita")
                    .execute()
        }

        setupMockRssFeed()
    }

    @Test
    fun shouldSendDiscordMsgIfNewMangaChapterIsFound() {
        var numOfDiscordCalls = 0
        var discordMsg = ""
        var discordEmbeds = emptyList<Embed>()
        val muFeedUrl = "http://localhost:15000/example_mu_feed.xml"
        val configBundle = ConfigBundle(muFeedUrl)
        val checker = MangaUpdatesChecker(dbConn, { msg: String, embeds: List<Embed> ->
            numOfDiscordCalls++
            discordMsg = msg
            discordEmbeds = embeds
        }, configBundle)
        val mangaRepository = MangaRepository(dbConn)
        mangaRepository.create("Bokutachi ga Yarimashita",
                "https://www.mangaupdates.com/series.html?id=121087", "121087",
                "http://example.org/")

        checker.run()

        assertEquals(1, numOfDiscordCalls)
        assertEquals("The following manga have new chapters: [Bokutachi ga Yarimashita]", discordMsg)
        assertEquals(listOf(Embed("Bokutachi ga Yarimashita", "http://example.org/", "[Nuwang]Bokutachi ga Yarimashita v.4 c.32")), discordEmbeds)

        checker.run()

        assertEquals(1, numOfDiscordCalls, "should only have called Discord once")
    }

    private fun setupDBConnector(): Jdbi {
        val dbHostname = System.getenv().getOrDefault("POSTGRES_HOST", "localhost")
        val dbUser = System.getenv().getOrDefault("POSTGRES_USER", "botkun_test_user")
        val dbPassword = System.getenv().getOrDefault("POSTGRES_PASSWORD", "secret")
        val dbName = System.getenv().getOrDefault("POSTGRES_DB", "botkuntestdb")
        val url = "jdbc:postgresql://$dbHostname/$dbName?user=$dbUser&password=$dbPassword&ssl=false"
        val jdbi = Jdbi.create(url)
                .installPlugin(PostgresPlugin())
                .installPlugin(KotlinPlugin())
                .registerColumnMapper(ZonedDateTimeMapper())
        return jdbi!!
    }

    private fun setupMockRssFeed() {
        val filePath = this.javaClass.getResource("/example_mu_feed.xml").toURI()
        val feedContents = FileUtils.readFileToString(File(filePath), "utf-8")
        wireMockRule.stubFor(get(urlPathMatching("/example_mu_feed.xml"))
                .willReturn(ok(feedContents)))
    }
}