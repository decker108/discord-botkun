package se.botkun.output

import com.github.tomakehurst.wiremock.client.WireMock.*
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import com.github.tomakehurst.wiremock.junit.WireMockRule
import org.junit.Rule
import org.junit.Test
import java.net.URL

class DiscordClientTest {

    @Rule
    @JvmField
    val wireMockRule = WireMockRule(WireMockConfiguration.options().port(15000))

    private val queryParams = "?wait=true"
    private val urlPath = "/api/webhooks/387205564c03/f77761b5cce0"
    private val url = "http://localhost:15000$urlPath$queryParams"

    @Test
    fun shouldCorrectlySendMessageWithoutEmbeds() {
        val expectedRequestJson = """
            {
                "content": "test message",
                "username": "Botkun",
                "embeds": []
            }
        """
        wireMockRule.stubFor(post(urlPathMatching(urlPath))
                .withQueryParam("wait", equalTo("true"))
                .withHeader("Content-Type", equalTo("application/json; charset=UTF-8"))
                .withRequestBody(equalToJson(expectedRequestJson))
                .willReturn(ok()))

        executeWebhook("test message", URL(url), emptyList())

        verify(1, postRequestedFor(urlPathMatching(urlPath)))
    }

    @Test
    fun shouldCorrectlySendMessageWithOneEmbed() {
        val expectedRequestJson = """
            {
              "content" : "test message",
              "username" : "Botkun",
              "embeds" : [ {
                "title" : "Aku no Hana",
                "url" : "http://example.org/",
                "description" : "test",
                "type": "rich"
              } ]
            }
        """
        wireMockRule.stubFor(post(urlPathMatching(urlPath))
                .withQueryParam("wait", equalTo("true"))
                .withHeader("Content-Type", equalTo("application/json; charset=UTF-8"))
                .withRequestBody(equalToJson(expectedRequestJson))
                .willReturn(ok()))

        executeWebhook("test message", URL(url), listOf(Embed("Aku no Hana", "http://example.org/", "test")))

        verify(1, postRequestedFor(urlPathMatching(urlPath)))
    }

    @Test
    fun shouldCorrectlySendMessageWithTwoEmbeds() {
        val expectedRequestJson = """
            {
              "content" : "test message",
              "username" : "Botkun",
              "embeds" : [ {
                "title" : "Aku no Hana",
                "url" : "http://example.org/",
                "description" : "test",
                "type": "rich"
              },{
                "title" : "Trail of Blood",
                "url" : "http://example.org/",
                "description" : "another test",
                "type": "rich"
              } ]
            }
        """
        wireMockRule.stubFor(post(urlPathMatching(urlPath))
                .withQueryParam("wait", equalTo("true"))
                .withHeader("Content-Type", equalTo("application/json; charset=UTF-8"))
                .withRequestBody(equalToJson(expectedRequestJson))
                .willReturn(ok()))

        executeWebhook("test message", URL(url), listOf(
                Embed("Aku no Hana", "http://example.org/", "test"),
                Embed("Trail of Blood", "http://example.org/", "another test")))

        verify(1, postRequestedFor(urlPathMatching(urlPath)))
    }
}