package se.botkun.persistence

import mu.KLogging
import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.core.kotlin.KotlinPlugin
import org.jdbi.v3.postgres.PostgresPlugin
import org.junit.Before
import org.junit.Test
import se.botkun.entity.FeedEntry
import se.botkun.entity.TrackedManga
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.*
import kotlin.math.absoluteValue
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class MangaRepositoryTests : KLogging() {
    private val exampleUrl = "http://example.com/"
    private val exampleSeriesId = "eac4ea38d83b8fb724a11f47019e9ed7f209e6be"
    private lateinit var mangaRepository: MangaRepository
    private lateinit var seriesId: String

    @Before
    fun setUp() {
        mangaRepository = MangaRepository(setupDBConnector())
        seriesId = Random().nextInt().absoluteValue.toString()
    }

    @Test
    fun insertTrackedManga() {
        val name = UUID.randomUUID().toString()
        val manga = mangaRepository.create(name, exampleUrl + name, seriesId)
        assertTrue { manga.id > 0 }
        assertEquals(manga.name, name)
        assertEquals(manga.url, exampleUrl + name)
    }

    @Test
    fun shouldFindMangaByName() {
        val name = UUID.randomUUID().toString()
        val manga = mangaRepository.create(name, exampleUrl + name, seriesId)
        val found = mangaRepository.findByName(name).get()
        assertTrue { manga.name == name }
        assert(found.name == name)
    }

    @Test
    fun shouldFindMangaById() {
        val name = UUID.randomUUID().toString()
        val created = mangaRepository.create(name, exampleUrl + name, seriesId)
        val mangaOpt = mangaRepository.findById(created.id)
        assert(mangaOpt.isPresent)
        assert(mangaOpt.get().name == name)
        assert(mangaOpt.get().id == created.id)
    }

    @Test
    fun deleteTrackedManga() {
        val name = UUID.randomUUID().toString()
        val manga = mangaRepository.create(name, exampleUrl + name, seriesId)
        mangaRepository.deleteById(manga.id)
        val mangaOpt = mangaRepository.findById(manga.id)
        assert(!mangaOpt.isPresent)
    }

    @Test(expected = RuntimeException::class)
    fun deleteNonExistentManga() {
        mangaRepository.deleteById(123)
    }

    @Test
    fun updateTrackedManga() {
        val name = UUID.randomUUID().toString()
        val newName = UUID.randomUUID().toString()
        val original = mangaRepository.create(name, exampleUrl + name, seriesId)
        val updated = mangaRepository.update(TrackedManga(original.id, newName, original.url, original.addDate,
                original.lastUpdateDate, original.seriesId))
        val foundOptional = mangaRepository.findByName(updated.name)
        assert(foundOptional.isPresent)
        foundOptional.ifPresent { found ->
            assert(updated.id == found.id)
            assert(found.name == newName)
        }
    }

    @Test
    fun shouldNotAllowMangaWithDuplicateUrls() {
        val name = UUID.randomUUID().toString()
        val manga = mangaRepository.create(name, exampleUrl + name, seriesId)
        assertTrue(manga.id > 0)
        assertFailsWith(RuntimeException::class) {
            mangaRepository.create(name, exampleUrl + UUID.randomUUID().toString(), seriesId)
        }
    }

    @Test
    fun shouldUpdateMangaIfFound() {
        val name = UUID.randomUUID().toString()
        val manga = mangaRepository.create(name, exampleUrl + name, seriesId)
        assertTrue { manga.id > 0 }

        val feedEntry = FeedEntry(exampleSeriesId, name, name,
                "http://localhost", ZonedDateTime.now(ZoneId.of("Z")), seriesId)
        mangaRepository.updateWithLatestRelease(feedEntry)

        val found = mangaRepository.findByName(name)
        assertTrue(found.isPresent)
        found.ifPresent {
            assertEquals(it.lastUpdateDate, feedEntry.updateDate)
        }
    }

    @Test
    fun shouldListAllManga() {
        val name = UUID.randomUUID().toString()
        val manga = mangaRepository.create(name, exampleUrl + name, seriesId)

        val list = mangaRepository.findAll()
        assertTrue(list.isNotEmpty())
        assertNotNull(list.find { it.id == manga.id })
    }
}

fun setupDBConnector(): Jdbi {
    val dbHostname = System.getenv().getOrDefault("POSTGRES_HOST", "localhost")
    val dbUser = System.getenv().getOrDefault("POSTGRES_USER", "botkun_test_user")
    val dbPassword = System.getenv().getOrDefault("POSTGRES_PASSWORD", "secret")
    val dbName = System.getenv().getOrDefault("POSTGRES_DB", "botkuntestdb")
    val url = "jdbc:postgresql://$dbHostname/$dbName?user=$dbUser&password=$dbPassword&ssl=false"
    val jdbi = Jdbi.create(url)
            .installPlugin(PostgresPlugin())
            .installPlugin(KotlinPlugin())
            .registerColumnMapper(ZonedDateTimeMapper())
    return jdbi!!
}