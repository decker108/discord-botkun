ALTER TABLE tracked_manga ADD COLUMN entryId VARCHAR(140) UNIQUE;

UPDATE tracked_manga SET entryId = SUBSTRING(url FROM 45);

ALTER TABLE tracked_manga ALTER COLUMN entryId SET NOT NULL;