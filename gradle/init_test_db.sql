create database botkuntestdb;
create role botkun_test_user with password 'secret';
grant connect on DATABASE botkuntestdb to botkun_test_user;
ALTER ROLE botkun_test_user WITH LOGIN;

\c botkuntestdb

CREATE TABLE tracked_manga(id SERIAL PRIMARY KEY NOT NULL, name VARCHAR(140) UNIQUE NOT NULL, url VARCHAR(140) UNIQUE NOT NULL, addDate TIMESTAMP WITH TIME ZONE DEFAULT NOW(), lastUpdateDate TIMESTAMP WITH TIME ZONE DEFAULT TIMESTAMP '1970-01-01');
GRANT SELECT,INSERT,UPDATE,DELETE ON tracked_manga TO botkun_test_user;
GRANT SELECT,USAGE,UPDATE ON tracked_manga_id_seq TO botkun_test_user;

CREATE TABLE tracked_anime(id SERIAL PRIMARY KEY NOT NULL, name VARCHAR(140) UNIQUE NOT NULL, running BOOL, url VARCHAR(140) UNIQUE NOT NULL, addDate TIMESTAMP WITH TIME ZONE DEFAULT NOW(), lastUpdateDate TIMESTAMP WITH TIME ZONE DEFAULT TIMESTAMP '1970-01-01');
GRANT SELECT,INSERT,UPDATE,DELETE ON tracked_anime TO botkun_test_user;
GRANT SELECT,USAGE,UPDATE ON tracked_anime_id_seq TO botkun_test_user;

CREATE TABLE anime_season(id SERIAL PRIMARY KEY NOT NULL, name VARCHAR(140) UNIQUE NOT NULL);
GRANT SELECT,INSERT,UPDATE,DELETE ON anime_season TO botkun_test_user;
GRANT SELECT,USAGE,UPDATE ON anime_season_id_seq TO botkun_test_user;