FROM openjdk:8u131-jre-alpine

LABEL maintainer="Decker108"

ADD ./build/distributions/discord-botkun.tar /

CMD ["/bin/sh", "/discord-botkun/bin/discord-botkun"]
